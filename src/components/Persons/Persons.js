import React, { Component } from 'react';
import Person from './Person/Person';

class Persons extends Component {
  render() {
    return this.props.persons.map(
      (person, idx) => (
        <Person 
              name={ person.name } 
              age={ person.age } 
              key={ person.id }
              click={ () => this.props.clicked(idx) } 
              changed={ (event) => this.props.changed(event, person.id) } />
      )
    )
  }
}

export default Persons;
