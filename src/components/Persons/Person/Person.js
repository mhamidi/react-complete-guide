import React, { Component } from 'react';
import './Person.css';

class Person extends Component {

  
  render() {
    const personStyle = {
      color: 'cornflowerblue'
    }
    return <div className="Person" style={ personStyle }>
            <p onClick={ this.props.click }>Hi, I'm a person, my name is { this.props.name } and I'm { this.props.age } years old.</p>
            <p>{ this.props.children }</p>
            <input type="text" onChange={ this.props.changed } value={ this.props.name } />
          </div>
  }
} 

export default Person;