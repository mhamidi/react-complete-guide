import React from 'react';
import { Button } from 'react-bootstrap';

const cockpit = (props) => (
  <div>
    <h1>Welcome to the course of React ...</h1>
    <h2>{ props.title }</h2>
    {/* <Button className="btn btn-primary" onClick={ this.switchNameHandler.bind(this, 'Hamidi') } >Switch Name</Button> */}
    {/* <Button className="btn btn-primary" onClick={ () => this.switchNameHandler( 'Hamidi!!') } >Switch Name</Button> */}
    <Button className="btn btn-info" onClick={ props.clicked } >Toggle Persons</Button>
  </div>
);

export default cockpit;
