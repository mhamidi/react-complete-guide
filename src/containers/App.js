import React, { Component } from 'react';
import './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit'

class App extends Component {
  state = {
    persons: [
      { id:"hsjksi", name: "Mohamed", age: "36" },
      { id:"psos8s", name: "Ibrahim", age: "29" },
      { id:"anjao5", name: "Latifa", age: "25" }
    ],
    otherStateProperty: "other state propertye value !!",
    showPersons: false
  }

  nameChangedHandler = (event, personid) => {
    const persons = [...this.state.persons];
    persons.forEach(person => {
      if (person.id === personid) {
        person.name = event.target.value;
      }
    });

    this.setState(
      { persons: persons }
    );
  }

  togglePersonsHandler = () => {
    const doesShowPersons = this.state.showPersons;
    this.setState({
      showPersons: !doesShowPersons
    });
  }

  deletePersonHandler = (index) => {
    // const persons = this.state.persons.slice();
    const persons = [...this.state.persons];
    persons.splice(index, 1);
    this.setState({
      persons: persons
    });
  }

  render() {
    let persons = null;
    if (this.state.showPersons) {
      persons = (
        <div>
          <Persons 
            persons={ this.state.persons}
            clicked={ this.deletePersonHandler }
            changed={ this.nameChangedHandler } />
        </div>    
      );
    } 

    return (
      <div className="App">
        <Cockpit clicked={ this.togglePersonsHandler } title={ this.props.title } />
        { persons }
        <hr/>
        <div>
          { this.state.otherStateProperty }
        </div>
      </div>
    );
    //return React.createElement('div', null, 'h2', 'Hello Mohamed to reactjs');
  }
}

export default App;
